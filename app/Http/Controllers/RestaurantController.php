<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;


class RestaurantController extends Controller
{
    public function findAll(): Response {
        return new Response([
            [
                'id' => 1,
                'name' => 'Pc Lanches',
                'products' => ['Coxinha', 'Pipoca', 'Cachorro Quente'],
                'typeProducts' => 'Brasileira',
                'photo' => "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSj2tz_xMSi-vVOEdKph1TcvFJ0UZTCjQwjrw&usqp=CAU",
                'description' => 'Boa comida'
            ],

            [
                'id' => 2,
                'name' => 'Pc Lanches',
                'products' => ['Coxinha', 'Pipoca', 'Cachorro Quente'],
                'typeProducts' => 'Brasileira',
                'photo' => "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSj2tz_xMSi-vVOEdKph1TcvFJ0UZTCjQwjrw&usqp=CAU",
                'description' => 'Boa comida'
            ],

            [
                'id' => 3,
                'name' => 'Pc Lanches',
                'products' => ['Coxinha', 'Pipoca', 'Cachorro Quente'],
                'typeProducts' => 'Brasileira',
                'photo' => "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSj2tz_xMSi-vVOEdKph1TcvFJ0UZTCjQwjrw&usqp=CAU",
                'description' => 'Boa comida'
            ],
        ]);
    }
}
