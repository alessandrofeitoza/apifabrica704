<?php


namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ProductController extends Controller
{
    public function findAll(): Response
    {
        return new Response([
            [
                'id' => 8,
                'name' => 'Heineken',
                'description' => '600ml',
                'price' => 10,
            ],
            [
                'id' => 9,
                'name' => 'Brahma',
                'description' => '600ml',
                'price' => 8,
            ],
            [
                'id' => 18,
                'name' => 'Bohemia',
                'description' => '600ml',
                'price' => 9,
            ],
            [
                'id' => 2,
                'name' => 'Kaiser',
                'description' => '600ml',
                'price' => 4.50,
            ],
            [
                'id' => 1,
                'name' => 'Bavaria',
                'description' => '600ml',
                'price' => 7,
            ],
            [
                'id' => 11,
                'name' => 'Skol',
                'description' => '600ml',
                'price' => 6,
            ],
        ]);
    }
}
